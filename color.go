package pewpew

import (
	"image/color"
	"math"
)

// Color describes an RGB color. It implements the color.Color interface.
type Color struct {
	Red, Green, Blue float32
}

// NewColor converts a color.Color. It ignores opacity (alpha).
func NewColor(c color.Color) Color { return newFromColor(c) }

// Converts from a color.Color, ignores alpha.
func newFromColor(c color.Color) Color {
	r, g, b, _ := c.RGBA()
	return Color{float32(r) / 0xFFFF, float32(g) / 0xFFFF, float32(b) / 0xFFFF}
}

// add adds a color to an existing color.
func (c Color) add(d Color) Color {
	return Color{c.Red + d.Red, c.Green + d.Green, c.Blue + d.Blue}
}

// scale scales a color by a factor.
func (c Color) scale(t float32) Color {
	return Color{c.Red * t, c.Green * t, c.Blue * t}
}

// RGBA returns the alpha-premultiplied red, green, blue and alpha values for
// the color. Each value ranges within [0, 0xFFFF] despite being represented
// with a uint32.
//
// The alpha value is always 0xFFFF.
func (c Color) RGBA() (r, g, b, a uint32) {
	c.clamp(0, 1)

	r = uint32(0xFFFF * c.Red)
	g = uint32(0xFFFF * c.Green)
	b = uint32(0xFFFF * c.Blue)
	a = 0xFFFF

	return
}

// attenuate reduces the color by a different factor per channel. Each channel
// should be within [0, 1].
func (c Color) attenuate(a Color) Color {
	return Color{
		c.Red * a.Red,
		c.Green * a.Green,
		c.Blue * a.Blue,
	}
}

// gammaCorrect applies a gamma factor.
//
// For a color (r, g, b) this yields (r^gamma, g^gamma, b^gamma).
func (c Color) gammaCorrect(gamma float32) Color {
	factor := 1 / gamma
	return Color{
		float32(math.Pow(float64(c.Red), float64(factor))),
		float32(math.Pow(float64(c.Green), float64(factor))),
		float32(math.Pow(float64(c.Blue), float64(factor))),
	}
}

// clamp restricts each channel of a color to be within a limit.
func (c Color) clamp(min, max float32) Color {
	return Color{
		clamp32(min, c.Red, max),
		clamp32(min, c.Green, max),
		clamp32(min, c.Blue, max),
	}
}

func clamp64(min, value, max float64) float64 {
	if value < min {
		return min
	} else if max < value {
		return max
	}
	return value
}

func clamp32(min, value, max float32) float32 {
	if value < min {
		return min
	} else if max < value {
		return max
	}
	return value
}

func (c Color) toUint8() (r, g, b uint8) {
	c = c.clamp(0, 0.9999)

	r = uint8(256 * c.Red)
	g = uint8(256 * c.Green)
	b = uint8(256 * c.Blue)

	return
}

// ColorAt returns the color's value, so that it can be used as a solid color
// Texturer.
func (c Color) ColorAt(u, v float64, p Pos) Color { return c }
