package pewpew

import (
	"math"
	"math/rand"
)

const (
	perlinPointCount = 256
)

type perlinNoise struct {
	randVecs            [perlinPointCount]Vec
	permX, permY, permZ [perlinPointCount]int
}

// Creates a perlin noise source.
func newPerlinNoise(rand *rand.Rand) *perlinNoise {
	out := &perlinNoise{
		randVecs: [perlinPointCount]Vec{},
		permX:    [perlinPointCount]int{},
		permY:    [perlinPointCount]int{},
		permZ:    [perlinPointCount]int{},
	}
	for i := 0; i < perlinPointCount; i++ {
		out.randVecs[i] = randomVOnUnitSphere(rand)
		out.permX[i] = i
		out.permY[i] = i
		out.permZ[i] = i
	}

	shuffle(out.permX[:])
	shuffle(out.permY[:])
	shuffle(out.permZ[:])

	return out
}

func shuffle[T any](v []T) {
	rand.Shuffle(len(v), func(i, j int) { v[i], v[j] = v[j], v[i] })
}

func (n *perlinNoise) Noise(p Pos) float64 {
	u := p.X - math.Floor(p.X)
	v := p.Y - math.Floor(p.Y)
	w := p.Z - math.Floor(p.Z)

	i := int(math.Floor(p.X))
	j := int(math.Floor(p.Y))
	k := int(math.Floor(p.Z))

	c := [8]Vec{
		n.randVecs[n.permX[i&255]^n.permY[j&255]^n.permZ[k&255]],
		n.randVecs[n.permX[i&255]^n.permY[j&255]^n.permZ[(k+1)&255]],
		n.randVecs[n.permX[i&255]^n.permY[(j+1)&255]^n.permZ[k&255]],
		n.randVecs[n.permX[i&255]^n.permY[(j+1)&255]^n.permZ[(k+1)&255]],
		n.randVecs[n.permX[(i+1)&255]^n.permY[j&255]^n.permZ[k&255]],
		n.randVecs[n.permX[(i+1)&255]^n.permY[j&255]^n.permZ[(k+1)&255]],
		n.randVecs[n.permX[(i+1)&255]^n.permY[(j+1)&255]^n.permZ[k&255]],
		n.randVecs[n.permX[(i+1)&255]^n.permY[(j+1)&255]^n.permZ[(k+1)&255]],
	}

	return triInterpolate(c, u, v, w)
}

func triInterpolate(c [8]Vec, u, v, w float64) float64 {
	uu0 := u * u * (3 - 2*u)
	vv0 := v * v * (3 - 2*v)
	ww0 := w * w * (3 - 2*w)

	uu1 := 1 - uu0
	vv1 := 1 - vv0
	ww1 := 1 - ww0

	return uu1*vv1*ww1*c[0].dot(Vec{u, v, w}) +
		uu1*vv1*ww0*c[1].dot(Vec{u, v, w - 1}) +
		uu1*vv0*ww1*c[2].dot(Vec{u, v - 1, w}) +
		uu1*vv0*ww0*c[3].dot(Vec{u, v - 1, w - 1}) +
		uu0*vv1*ww1*c[4].dot(Vec{u - 1, v, w}) +
		uu0*vv1*ww0*c[5].dot(Vec{u - 1, v, w - 1}) +
		uu0*vv0*ww1*c[6].dot(Vec{u - 1, v - 1, w}) +
		uu0*vv0*ww0*c[7].dot(Vec{u - 1, v - 1, w - 1})
}
