package pewpew

import (
	"math"
)

// A Sphere is a spherical-shaped object in a scene.
type Sphere struct {
	center   Pos
	radius   float64
	material Scatterer
}

// NewSphere creates a sphere object.
func NewSphere(x, y, z, radius float64, material Scatterer) *Sphere {
	return &Sphere{
		center:   Pos{x, y, z},
		radius:   radius,
		material: material,
	}
}

// Intersect checks for an intersection of a ray with a sphere, within bounds.
func (s *Sphere) Intersect(r Ray, mMin float64, mMax float64) (hit *Hit, material Scatterer, ok bool) {
	offset := s.center.to(r.Origin)
	a := r.Dir.lengthSquared()
	bHalf := r.Dir.dot(offset)
	c := offset.lengthSquared() - s.radius*s.radius

	// Check for a hit
	descriminant := bHalf*bHalf - a*c
	if descriminant < 0 {
		return // no hit
	}

	// Find nearest root within bounds
	sqrtDesc := math.Sqrt(descriminant)
	root := (-bHalf - sqrtDesc) / a
	if root < mMin || mMax < root {
		root = (-bHalf + sqrtDesc) / a
		if root < mMin || mMax < root {
			return // no hit within bounds
		}
	}

	// Calculate hit info
	hitP := r.at(root)
	outwardNormal := s.center.to(hitP).scale(1 / s.radius)
	u, v := getSphereSurfaceCoords(outwardNormal)
	hit = NewHitFromOutwardNormal(hitP, outwardNormal, root, u, v, r)

	return hit, s.material, true
}

// BoundingBox generates the bounding box for a sphere.
func (s *Sphere) BoundingBox(time0, time1 float64) AABB {
	return AABB{
		Min: s.center.subV(Vec{s.radius, s.radius, s.radius}),
		Max: s.center.addV(Vec{s.radius, s.radius, s.radius}),
	}
}

// Returns surface coordinates of a point on a spherical surface along the
// direction dir.
//
// u is the angle [0,1] around the Y-axis from X=-1.
// v is the angle [0,1] from Y=-1 to Y=1.
func getSphereSurfaceCoords(dir Vec) (u, v float64) {
	dir = dir.norm() // get unit vector of dir
	theta := math.Acos(-dir.Y)
	phi := math.Atan2(-dir.Z, dir.X) + math.Pi
	return phi / (2 * math.Pi), theta / math.Pi
}

// A MovingSphere is a spherical-shaped object with motion.
type MovingSphere struct {
	center       Pos
	radius       float64
	material     Scatterer
	shift        Vec
	time0, time1 float64
}

// NewMovingSphere creates a moving sphere object. The duration must be greater
// than zero.
//
// The sphere's position over time is a linear interpretation between:
//
//	t = 0:         position = (x, y, z)
//	t = duration:  position = (x, y, z) + shift
func NewMovingSphere(x, y, z, radius float64, material Scatterer, shift Vec, time0, time1 float64) *MovingSphere {
	if !(time1-time0 > 0) {
		panic("time1 must be greater than time0")
	}
	return &MovingSphere{
		center:   Pos{x, y, z},
		radius:   radius,
		material: material,
		shift:    shift,
		time0:    time0,
		time1:    time1,
	}
}

// Intersect checks for an intersection of a ray with a sphere, within bounds.
func (s *MovingSphere) Intersect(r Ray, mMin float64, mMax float64) (hit *Hit, material Scatterer, ok bool) {
	center := s.center.addV(s.shift.scale((r.Time - s.time0) / (s.time1 - s.time0)))
	offset := center.to(r.Origin)
	a := r.Dir.lengthSquared()
	bHalf := r.Dir.dot(offset)
	c := offset.lengthSquared() - s.radius*s.radius

	// Check for a hit
	descriminant := bHalf*bHalf - a*c
	if descriminant < 0 {
		return // no hit
	}

	// Find nearest root within bounds
	sqrtDesc := math.Sqrt(descriminant)
	root := (-bHalf - sqrtDesc) / a
	if root < mMin || mMax < root {
		root = (-bHalf + sqrtDesc) / a
		if root < mMin || mMax < root {
			return // no hit within bounds
		}
	}

	// Calculate hit info
	hitP := r.at(root)
	outwardNormal := center.to(hitP).scale(1 / s.radius)
	u, v := getSphereSurfaceCoords(outwardNormal)
	hit = NewHitFromOutwardNormal(hitP, outwardNormal, root, u, v, r)

	return hit, s.material, true
}

// BoundingBox generates the bounding box for a moving sphere.
func (s *MovingSphere) BoundingBox(time0, time1 float64) AABB {
	center0 := s.center.addV(s.shift.scale((time0 - s.time0) / (s.time1 - s.time0)))
	center1 := s.center.addV(s.shift.scale((time1 - s.time0) / (s.time1 - s.time0)))
	box0 := AABB{
		Min: center0.subV(Vec{s.radius, s.radius, s.radius}),
		Max: center0.addV(Vec{s.radius, s.radius, s.radius}),
	}
	box1 := AABB{
		Min: center1.subV(Vec{s.radius, s.radius, s.radius}),
		Max: center1.addV(Vec{s.radius, s.radius, s.radius}),
	}
	return surroundingBox(box0, box1)
}
