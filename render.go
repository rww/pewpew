package pewpew

import (
	"image/color"
	"math"
	"math/rand"

	"codeberg.org/rww/pewpew/internal/random"
)

// A Render is used to ray-trace a scene and produce an image.
type Render struct {
	camera     *Camera
	tangibles  Intersecter
	background Color
}

// NewRender creates a renderer for creating an image of a scene.
func NewRender(camera *Camera, tangibles []Intersecter, background color.Color) Render {
	return Render{
		camera:     camera,
		tangibles:  newBVHNode(tangibles, camera.startTime, camera.endTime),
		background: newFromColor(background),
	}
}

// Do runs a render, producing an image.
func (ren Render) Do(width, height, samplesPerPixel, routineAmount int) Image {
	const (
		maxBounces = 50
		gamma      = 2
	)

	samplesPerRoutine := divideCeil(samplesPerPixel, routineAmount)
	ch := make(chan Image, routineAmount)
	for i := 0; i < routineAmount; i++ {
		go func() {
			ch <- ren.do(width, height, samplesPerRoutine, maxBounces, gamma)
		}()
	}

	img := newImage(width, height, gamma)
	for i := 0; i < routineAmount; i++ {
		weight := 1 / float32(i+1)
		img.combine(<-ch, weight)
	}

	return img
}

func (ren Render) do(width, height, samples, maxBounces int, gamma float32) Image {
	rnd := random.New()
	img := newImage(width, height, gamma)

	for j := 0; j < height; j++ {
		for i := 0; i < width; i++ {
			c := Color{0, 0, 0}

			for s := 0; s < samples; s++ {
				u := (float64(i) + rnd.Float64()) / float64(img.width-1)
				v := (float64(img.height-j-1) + rnd.Float64()) / float64(img.height-1)

				r := ren.camera.getRay(rnd, u, v)

				c = c.add(rayTrace(rnd, ren.tangibles, ren.background, r, maxBounces))
			}

			img.set(i, j, c, samples)
		}
	}

	return img
}

// Determines the color detected by a ray, given a scene.
func rayTrace(rnd *rand.Rand, tangibles Intersecter, background Color, r Ray, allowedDepth int) Color {
	// Check for maximum recusion depth
	if allowedDepth <= 0 {
		return Color{0, 0, 0} // the darkness of infinitely-removed space...
	}

	// Check for collisions with world
	// min=0.0001 prevents hits very near zero to get rid of 'shadow acne'
	if hit, material, ok := tangibles.Intersect(r, 0.0001, math.MaxFloat64); ok {
		emitted := material.Emit(hit.U, hit.V, hit.Point)

		attenuation, scattered, ok := material.Scatter(rnd, r, hit)
		if !ok {
			return emitted // absorbed, only return emission
		}

		return rayTrace(rnd, tangibles, background, scattered, allowedDepth-1).
			attenuate(attenuation).
			add(emitted)
	}

	// Background color otherwise
	return background
}

// Integer division, rounded up: ceil(n/d).
func divideCeil(n, d int) int {
	out := n / d
	if out*d < n {
		out++
	}
	return out
}
