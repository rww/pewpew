package pewpew

import (
	"image"
	"math"
	"math/rand"
)

// Texturer is used to map colors, images, patterns, etc to an object's surface.
type Texturer interface {
	ColorAt(u, v float64, p Pos) Color
}

// A CheckerTexture is a Texturer that tiles two textures in a cubic grid
// through 3D space.
type CheckerTexture struct {
	odd, even Texturer
	density   float64 // how
}

// NewCheckerTexture creates a new CheckerTexture that patterns the two given
// Texturers.
func NewCheckerTexture(t1, t2 Texturer) *CheckerTexture {
	return &CheckerTexture{t1, t2, 10.0}
}

// ColorAt returns one of the CheckerTexture's two Texturers, depending on the
// position in 3D space.
func (ct *CheckerTexture) ColorAt(u, v float64, p Pos) Color {
	parity := math.Sin(ct.density*p.X) * math.Sin(ct.density*p.Y) * math.Sin(ct.density*p.Z)
	if parity < 0 {
		return ct.odd.ColorAt(u, v, p)
	}
	return ct.even.ColorAt(u, v, p)
}

// A NoiseTexture using Perlin noise.
type NoiseTexture struct {
	scale  float64
	perlin *perlinNoise
}

// NewNoiseTexture creates a new noise texture that uses Perlin noise.
func NewNoiseTexture(scale float64) *NoiseTexture {
	return &NoiseTexture{
		scale:  scale,
		perlin: newPerlinNoise(rand.New(rand.NewSource(1))),
	}
}

// ColorAt returns a greyscale color at a position using Perlin noise.
func (nt *NoiseTexture) ColorAt(u, v float64, p Pos) Color {
	// perlin noise is from -1 to 1, this is scaled up to 0-1 for the color
	// scaling to prevent NaN's later
	t := 0.5 * (1 + nt.perlin.Noise(p.scale(nt.scale)))
	return Color{1, 1, 1}.scale(float32(t))
}

// A ImageTexture maps an image onto a surface as the texture coloring.
type ImageTexture struct {
	img              image.Image
	width, height    float64
	offsetX, offsetY int
}

func NewImageTexture(img image.Image) *ImageTexture {
	return &ImageTexture{
		img:     img,
		width:   float64(img.Bounds().Dx()),
		height:  float64(img.Bounds().Dy()),
		offsetX: img.Bounds().Min.X,
		offsetY: img.Bounds().Min.Y,
	}
}

// ColorAt returns the color of an image at some surface coordinates (u,v).
func (it *ImageTexture) ColorAt(u, v float64, p Pos) Color {
	if it.img == nil {
		return Color{0, 1, 1} // cyan as debug color for a missing image
	}

	u = clamp64(0, u, 0.99999)
	v = 1 - clamp64(0.00001, v, 1) // Flip v to image coordinates

	i := int(u * it.width)
	j := int(v * it.height)

	r, g, b, _ := it.img.At(i+it.offsetX, j+it.offsetY).RGBA()

	return Color{float32(r) / 0xFFFF, float32(g) / 0xFFFF, float32(b) / 0xFFFF}
}
