package pewpew

import (
	"math"
	"math/rand"
)

// A LambertianMaterial is a basic diffuser material.
type LambertianMaterial struct {
	albedo Texturer
}

// NewLambertianMaterial creates a new LambertianMaterial with the given albedo
// texture.
//
// The albedo is applied multiplicatively to scattered rays. Each of its entries
// should be within [0, 1].
func NewLambertianMaterial(albedo Texturer) *LambertianMaterial {
	return &LambertianMaterial{albedo: albedo}
}

// Scatter gives the color attenuation (scale factor) and scatter direction
// for a ray that intersected a LambertianMaterial.
func (lm *LambertianMaterial) Scatter(rnd *rand.Rand, incoming Ray, hit *Hit) (attenuation Color, scattered Ray, ok bool) {
	scatterDir := hit.Normal.add(randomVOnUnitSphere(rnd))
	if scatterDir.isNearZero() {
		scatterDir = hit.Normal // prevent divide by zeros later
	}
	attenuation = lm.albedo.ColorAt(hit.U, hit.V, hit.Point)
	scattered = newRay(hit.Point, scatterDir, incoming.Time) //TODO incorporate speed of light
	ok = true
	return
}

// Emit returns no emissions (black) for a LambertianMaterial.
func (lm *LambertianMaterial) Emit(u, v float64, p Pos) Color {
	return Color{0, 0, 0}
}

// A MetalMaterial is a metal material.
type MetalMaterial struct {
	albedo Texturer
	fuzz   float64
}

// NewMetalMaterial creates a new MetalMaterial with the given albedo color.
//
// The albedo is applied multiplicatively to scattered rays. Each of its entries
// should be within [0, 1]. The fuzzing factor should be within [0, 1] as well.
func NewMetalMaterial(albedo Texturer, fuzz float64) *MetalMaterial {
	if fuzz < 0 {
		fuzz = 0
	} else if 1 < fuzz {
		fuzz = 1
	}
	return &MetalMaterial{
		albedo: albedo,
		fuzz:   fuzz,
	}
}

// Scatter gives the color attenuation (scale factor) and scatter direction
// for a ray that intersected a MetalMaterial.
func (mm *MetalMaterial) Scatter(rnd *rand.Rand, incoming Ray, hit *Hit) (attenuation Color, scattered Ray, ok bool) {
	reflected := incoming.Dir.norm().reflect(hit.Normal)
	fuzzed := reflected.add(randomVInUnitSphere(rnd).scale(mm.fuzz)) // add some random 'fuzz' to the reflected ray direction
	scattered = newRay(hit.Point, fuzzed, incoming.Time)             //TODO incorporate speed of light
	attenuation = mm.albedo.ColorAt(hit.U, hit.V, hit.Point)
	ok = fuzzed.dot(hit.Normal) > 0
	return
}

// Emit returns no emissions (black) for a MetalMaterial.
func (mm *MetalMaterial) Emit(u, v float64, p Pos) Color {
	return Color{0, 0, 0}
}

// A DielectricMaterial describes a transparent material that refracts and
// reflects.
type DielectricMaterial struct {
	refractiveIndex float64
}

// NewDielectricMaterial creates a new DielectricMaterial.
func NewDielectricMaterial(indexOfRefraction float64) *DielectricMaterial {
	return &DielectricMaterial{refractiveIndex: indexOfRefraction}
}

// Scatter gives the color attenuation (scale factor) and scatter direction
// for a ray that intersected a DielectricMaterial.
func (dm *DielectricMaterial) Scatter(rnd *rand.Rand, incoming Ray, hit *Hit) (attenuation Color, scattered Ray, ok bool) {
	ratio := dm.refractRatio(hit)

	inDir := incoming.Dir.norm()
	cosTheta := math.Min(inDir.rev().dot(hit.Normal), 1)
	sinTheta := math.Sqrt(1 - cosTheta*cosTheta)

	cannotRefract := (ratio*sinTheta > 1.0)
	var outDir Vec
	if cannotRefract || reflectance(cosTheta, ratio) > rnd.Float64() {
		outDir = inDir.reflect(hit.Normal)
	} else {
		outDir = refract(inDir, hit.Normal, ratio)
	}

	attenuation = Color{1, 1, 1}                         // no attenuation
	scattered = newRay(hit.Point, outDir, incoming.Time) //TODO incorporate speed of light
	ok = true
	return
}

func (dm *DielectricMaterial) refractRatio(hit *Hit) float64 {
	const ambientRefractIndex = 1.0
	if hit.OutsideFace {
		return ambientRefractIndex / dm.refractiveIndex
	}
	return dm.refractiveIndex / ambientRefractIndex
}

// Determines the refraction direction. Both incident and normal should be unit
// vectors.
func refract(incident Vec, normal Vec, refractRatio float64) Vec {
	cosTheta := math.Min(incident.rev().dot(normal), 1)
	outPerpend := normal.scale(cosTheta).add(incident).scale(refractRatio)
	outParallel := normal.scale(
		-math.Sqrt(math.Abs(1 - outPerpend.lengthSquared())))
	return outPerpend.add(outParallel)
}

// Calculates reflectance of a dielectric using the Schlick approximation.
func reflectance(cosine, refractRatio float64) float64 {
	r0 := (1 - refractRatio) / (1 + refractRatio)
	r0 = r0 * r0
	return r0 + (1-r0)*math.Pow(1-cosine, 5)
}

// Emit returns no emissions (black) for a DielectricMaterial.
func (dm *DielectricMaterial) Emit(u, v float64, p Pos) Color {
	return Color{0, 0, 0}
}

// An EmissionMaterial describes a material that only gives off light, it does
// not scatter.
type EmissionMaterial struct {
	emission Texturer
}

// NewEmissionMaterial creates an emission-only material with the given emission
// texture.
func NewEmissionMaterial(emission Texturer) *EmissionMaterial {
	return &EmissionMaterial{emission: emission}
}

// Scatter always gives complete absorption for an EmissionMaterial.
func (em *EmissionMaterial) Scatter(rnd *rand.Rand, incoming Ray, hit *Hit) (attenuation Color, scattered Ray, ok bool) {
	return // Absorbed, zero color and okay=false
}

// Emit gives the emission color for an EmissionMaterial.
func (em *EmissionMaterial) Emit(u, v float64, p Pos) Color {
	return em.emission.ColorAt(u, v, p)
}

// Used to add emission to an existing material.
type emissionWrapper struct {
	emission Texturer
	material Scatterer
}

// AddEmission wraps an existing material with an additional emission texture.
func AddEmission(material Scatterer, emission Texturer) Scatterer {
	return &emissionWrapper{
		emission: emission,
		material: material,
	}
}

func (ew *emissionWrapper) Scatter(rnd *rand.Rand, incoming Ray, hit *Hit) (attenuation Color, scattered Ray, ok bool) {
	return ew.material.Scatter(rnd, incoming, hit)
}

func (ew *emissionWrapper) Emit(u, v float64, p Pos) Color {
	return ew.emission.ColorAt(u, v, p).
		add(ew.material.Emit(u, v, p))
}
