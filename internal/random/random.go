package random

import (
	"math/rand"
	"sync/atomic"
	"time"
)

var globalSeed atomic.Int64

func init() {
	globalSeed.Store(time.Now().UnixMicro())
}

// New creates a new rand.Rand with an auto-incrementing seed. It is safe for
// concurrent use.
func New() *rand.Rand {
	seed := globalSeed.Add(1)
	return rand.New(rand.NewSource(seed))
}
