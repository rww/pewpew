package pewpew

import (
	"math"
	"math/rand"
)

// Pos is a 3D position.
type Pos struct {
	X, Y, Z float64
}

// addV adds a vector to a position.
func (p Pos) addV(v Vec) Pos { return Pos{p.X + v.X, p.Y + v.Y, p.Z + v.Z} }

// subV subtracts a vector from a position.
func (p Pos) subV(v Vec) Pos { return Pos{p.X - v.X, p.Y - v.Y, p.Z - v.Z} }

// to returns the vector going from p to end.
func (p Pos) to(end Pos) Vec { return Vec{end.X - p.X, end.Y - p.Y, end.Z - p.Z} }

// scales a position.
func (p Pos) scale(t float64) Pos { return Pos{t * p.X, t * p.Y, t * p.Z} }

// A Ray is a direction from a point.
type Ray struct {
	Origin Pos
	Dir    Vec
	Time   float64
}

// newRay makes a new 3D ray.
func newRay(origin Pos, direction Vec, time float64) Ray {
	return Ray{
		Origin: origin,
		Dir:    direction,
		Time:   time,
	}
}

// newRayToPosition makes a new ray going from start to end.
func newRayToPosition(start, end Pos, time float64) Ray {
	return Ray{
		Origin: start,
		Dir:    start.to(end),
		Time:   time,
	}
}

// Gets a point along a ray, at m multiples of the ray's length.
func (r Ray) at(m float64) Pos { return r.Origin.addV(r.Dir.scale(m)) }

// Vec is a 3D vector. A zero struct is a zero-length vector.
type Vec struct {
	X, Y, Z float64
}

// scale scales the vector.
func (v Vec) scale(t float64) Vec { return Vec{t * v.X, t * v.Y, t * v.Z} }

// rev reverses the vector.
func (v Vec) rev() Vec { return v.scale(-1) }

// add adds another vector.
func (v Vec) add(u Vec) Vec { return Vec{v.X + u.X, v.Y + u.Y, v.Z + u.Z} }

// sub subtracts another vector.
func (v Vec) sub(u Vec) Vec { return Vec{v.X - u.X, v.Y - u.Y, v.Z - u.Z} }

// length gets the length of the vector.
func (v Vec) length() float64 { return math.Sqrt(v.lengthSquared()) }

// lengthSquared gets the squared length of the vector.
func (v Vec) lengthSquared() float64 {
	return v.X*v.X + v.Y*v.Y + v.Z*v.Z
}

// isNearZero checks if a vector is very close to zero in all dimensions.
func (v Vec) isNearZero() bool {
	const bound = 1e-8
	return math.Abs(v.X) < bound && math.Abs(v.Y) < bound && math.Abs(v.Z) < bound
}

// reflect bounces a vector off a normal surface n.
func (v Vec) reflect(normal Vec) Vec {
	bLen := 2 * v.dot(normal)
	return v.sub(normal.scale(bLen))
}

// norm returns the unit vector (direction).
func (v Vec) norm() Vec { return v.scale(1 / v.length()) }

// Returns the dot product of two vectors.
func (v Vec) dot(w Vec) float64 { return v.X*w.X + v.Y*w.Y + v.Z*w.Z }

// Returns the cross product of two vectors.
func (v Vec) crossProduct(w Vec) Vec {
	return Vec{
		v.Y*w.Z - v.Z*w.Y,
		v.Z*w.X - v.X*w.Z,
		v.X*w.Y - v.Y*w.X,
	}
}

// Rotates a vector about an axis.
//
// Reference: https://en.wikipedia.org/wiki/Rodrigues%27_rotation_formula
func (v Vec) rotateAbout(axis Vec, angleDegrees float64) Vec {
	theta := degToRad(angleDegrees)
	cosTheta := math.Cos(theta)
	sinTheta := math.Sin(theta)
	sinHalfTheta := math.Sin(theta / 2)

	axis = axis.norm() // normalize

	return v.scale(cosTheta).
		add(axis.crossProduct(v).scale(sinTheta)).
		// Use 2*sin(θ/2)^2 = 1 - cos(θ) because it is more accurate near multiples of 2*pi
		add(axis.scale(axis.dot(v) * 2 * sinHalfTheta * sinHalfTheta))
}

// randomVInUnitSquare creates a uniformly-distributed random vector within a
// unit square on the X-Y plane: x,y in [-1,1).
func randomVInUnitSquare(rnd *rand.Rand) Vec {
	return Vec{
		2*rnd.Float64() - 1,
		2*rnd.Float64() - 1,
		0,
	}
}

// randomVInUnitCircle creates a uniformly-distributed random vector within a
// unit circle on the X-Y plane.
func randomVInUnitCircle(rnd *rand.Rand) Vec {
	for {
		// Sample until within the unit circle
		v := randomVInUnitSquare(rnd)
		if v.lengthSquared() < 1 {
			return v
		}
	}
}

// randomVInUnitCube creates a uniformly-distributed random vector within the
// unit cube.
func randomVInUnitCube(rnd *rand.Rand) Vec {
	return Vec{
		2*rnd.Float64() - 1,
		2*rnd.Float64() - 1,
		2*rnd.Float64() - 1,
	}
}

// randomVInUnitSphere creates a uniformly-distributed random vector within the
// unit sphere.
func randomVInUnitSphere(rnd *rand.Rand) Vec {
	for {
		// Sample until within the unit sphere
		v := randomVInUnitCube(rnd)
		if v.lengthSquared() < 1 {
			return v
		}
	}
}

// randomVOnUnitSphere creates a uniformly-distributed random vector over the
// surface of the unit sphere.
func randomVOnUnitSphere(rnd *rand.Rand) Vec {
	return randomVInUnitSphere(rnd).norm()
}
