package main

import (
	"flag"
	"fmt"
	"image/color"
	"image/jpeg"
	"math"
	"math/rand"
	"os"
	"runtime"
	"runtime/pprof"
	"time"

	"codeberg.org/rww/pewpew"
)

func main() {
	var (
		outputFilePath  = flag.String("out", "./image.png", "output file path (*.png)")
		enableProfiling = flag.Bool("profile", false, "enable CPU and memory profiling")
	)
	flag.Parse()

	// Do profiling
	if *enableProfiling {
		done, err := doProfiling("./cpu.prof", "./mem.prof")
		if err != nil {
			fmt.Println(err)
			os.Exit(1)
		}
		defer done()
	}

	// Image
	aspectRatio := 16.0 / 9.0
	imageWidth := 800
	imageHeight := int(float64(imageWidth) / aspectRatio)
	samplesPerPixel := 100
	routineAmount := 8

	// Scene
	camera, world, background := randomScene(aspectRatio)
	//camera, world, background := twoSphereScene(aspectRatio)

	// Render
	image := pewpew.NewRender(camera, world, background).
		Do(imageWidth, imageHeight, samplesPerPixel, routineAmount)

	// Save to file
	if err := image.AsPNG(*outputFilePath); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}

func doProfiling(cpuProfileFile, memProfileFile string) (done func(), err error) {
	f, err := os.Create(cpuProfileFile)
	if err != nil {
		return
	}

	if err = pprof.StartCPUProfile(f); err != nil {
		defer f.Close()
		return
	}

	start := time.Now()

	return func() {
		fmt.Printf("duration = %s\n", time.Since(start))

		fMem, _ := os.Create(memProfileFile)
		defer fMem.Close()
		runtime.GC()
		_ = pprof.WriteHeapProfile(fMem)

		pprof.StopCPUProfile()
		f.Close()
	}, nil
}

func dist(p1, p2 pewpew.Pos) float64 {
	dX := p2.X - p1.X
	dY := p2.Y - p1.Y
	dZ := p2.Z - p1.Z
	return math.Sqrt(dX*dX + dY*dY + dZ*dZ)
}

func randomDarkColor() pewpew.Color {
	return pewpew.Color{
		// Squared rands biases away from light colors
		Red:   rand.Float32() * rand.Float32(),
		Green: rand.Float32() * rand.Float32(),
		Blue:  rand.Float32() * rand.Float32(),
	}
}

func randomBrightColor() pewpew.Color {
	return pewpew.Color{
		Red:   0.5 + 0.5*rand.Float32(),
		Green: 0.5 + 0.5*rand.Float32(),
		Blue:  0.5 + 0.5*rand.Float32(),
	}
}

func randomScene(aspectRatio float64) (c *pewpew.Camera, w []pewpew.Intersecter, b color.Color) {
	// Scene
	background := color.RGBA{128, 179, 255, 255}
	world := []pewpew.Intersecter{}

	checkerPattern := pewpew.NewCheckerTexture(
		pewpew.Color{Red: 0.2, Green: 0.3, Blue: 0.1},
		pewpew.Color{Red: 0.9, Green: 0.9, Blue: 0.9},
	)
	groundMat := pewpew.NewLambertianMaterial(checkerPattern)
	world = append(world, pewpew.NewSphere(0, -1000, 0, 1000, groundMat))

	for a := -11; a < 11; a++ {
		for b := -11; b < 11; b++ {
			rollForMat := rand.Float64()
			center := pewpew.Pos{
				X: float64(a) + 0.9*rand.Float64(),
				Y: 0.2,
				Z: float64(b) + 0.9*rand.Float64(),
			}

			if dist(center, pewpew.Pos{X: 4, Y: 0.2, Z: 0}) > 0.9 {
				switch {
				case rollForMat < 0.8: // Diffuse, moving
					sphereMat := pewpew.NewLambertianMaterial(randomDarkColor())
					world = append(world, pewpew.NewMovingSphere(
						center.X, center.Y, center.Z,
						0.2,
						sphereMat,
						pewpew.Vec{X: 0, Y: 0.5 * rand.Float64(), Z: 0},
						0, 1,
					))
				case rollForMat < 0.95: // Metal
					sphereMat := pewpew.NewMetalMaterial(randomBrightColor(), 0.5*rand.Float64())
					world = append(world, pewpew.NewSphere(
						center.X, center.Y, center.Z,
						0.2,
						sphereMat,
					))
				default: // Glass
					sphereMat := pewpew.NewDielectricMaterial(1.5)
					world = append(world, pewpew.NewSphere(
						center.X, center.Y, center.Z,
						0.2,
						sphereMat,
					))
				}
			}
		}
	}

	mat1 := pewpew.NewDielectricMaterial(1.5)
	mat2 := pewpew.NewLambertianMaterial(pewpew.Color{Red: 0.4, Green: 0.2, Blue: 0.1})
	mat3 := pewpew.NewMetalMaterial(pewpew.Color{Red: 0.7, Green: 0.6, Blue: 0.5}, 0)

	world = append(world,
		pewpew.NewSphere(0, 1, 0, 1, mat1),
		pewpew.NewSphere(-4, 1, 0, 1, mat2),
		pewpew.NewSphere(4, 1, 0, 1, mat3))

	// Camera
	camera := pewpew.NewCamera(
		pewpew.Pos{X: 13, Y: 2, Z: 3}, pewpew.Pos{X: 0, Y: 0, Z: 0},
		pewpew.FieldOfView(aspectRatio, 20),
		pewpew.Aperture(0.1),
		pewpew.ShutterSpeed(0, 1),
		pewpew.Focus(10),
	)

	return camera, world, background
}

func twoSphereScene(aspectRatio float64) (c *pewpew.Camera, w []pewpew.Intersecter, b color.Color) {
	file, err := os.Open("./res/earthmap.jpg")
	if err != nil {
		panic(err)
	}
	earthImg, err := jpeg.Decode(file)
	if err != nil {
		panic(err)
	}

	// Scene
	background := color.RGBA{128, 179, 255, 255}
	world := []pewpew.Intersecter{}

	surface := pewpew.NewLambertianMaterial(pewpew.NewNoiseTexture(4))
	earth := pewpew.NewLambertianMaterial(pewpew.NewImageTexture(earthImg))
	world = append(world, pewpew.NewSphere(0, -1000, 0, 1000, surface))
	world = append(world, pewpew.NewSphere(0, 2, 0, 2, earth))

	// Camera
	camera := pewpew.NewCamera(
		pewpew.Pos{X: 13, Y: 2, Z: 3}, pewpew.Pos{X: 0, Y: 0, Z: 0},
		pewpew.FieldOfView(aspectRatio, 20),
	)

	return camera, world, background
}
