package pewpew

import (
	"fmt"
	"image"
	"image/color"
	"image/png"
	"os"
)

// An Image is the data from a rendered view of a Scene. It implements the
// image.Image interface.
type Image struct {
	width  int
	height int
	gamma  float32

	// pix holds the image's pixels, in RGBA order. The pixel at (x, y) starts
	// at pix[y*stride + x*3].
	stride int // stride in bytes between vertically adjacent pixels
	pix    []float32
}

func newImage(width, height int, gamma float32) Image {
	return Image{
		width:  width,
		height: height,
		gamma:  gamma,

		stride: 3 * width,
		pix:    make([]float32, 3*width*height),
	}
}

// ColorModel returns the Image's color model.
func (img *Image) ColorModel() color.Model { return color.NRGBAModel }

// Bounds returns the domain for which At can return non-zero color.
func (img *Image) Bounds() image.Rectangle {
	return image.Rect(0, 0, img.width, img.height)
}

// At returns the color of the pixel at (x, y).
func (img *Image) At(x, y int) color.Color { return img.at(x, y) }

func (img *Image) at(x, y int) color.NRGBA {
	offset := img.pixOffset(x, y)
	s := img.pix[offset : offset+3 : offset+3] // apparently better performance: see https://golang.org/issue/27857

	r, g, b := Color{s[0], s[1], s[2]}.
		gammaCorrect(img.gamma).
		toUint8()

	return color.NRGBA{r, g, b, 255}
}

func (img *Image) set(x, y int, c Color, samples int) {
	offset := img.pixOffset(x, y)
	s := img.pix[offset : offset+3 : offset+3] // apparently better performance: see https://golang.org/issue/27857
	c = c.scale(1 / float32(samples))

	s[0] = c.Red
	s[1] = c.Green
	s[2] = c.Blue
}

func (img *Image) pixOffset(x, y int) int { return y*img.stride + x*3 }

// Combines data from a source image into an existing image. The caller is
// responsible for deallocating the source image.
//
// Images must have the same width and height. The weight changes what
// proportion of the combined image comes from the source image. It must be
// between 0 and 1.
func (img *Image) combine(s Image, weight float32) {
	if img.width != s.width || img.height != s.height {
		panic("Image.combine: images are not the same size")
	}

	for i := 0; i < len(img.pix); i++ {
		img.pix[i] = (1-weight)*img.pix[i] + weight*s.pix[i]
	}
}

// AsPNG saves the image to in the .png format.
func (img *Image) AsPNG(filePath string) error {
	f, err := os.Create(filePath)
	if err != nil {
		return fmt.Errorf("could not create file '%s': %w", filePath, err)
	}
	defer f.Close()

	err = png.Encode(f, img)
	if err != nil {
		return fmt.Errorf("could encode image to file: %w", err)
	}

	return nil
}

// AsPPM saves the image in the P3 .ppm format.
func (img *Image) AsPPM(filePath string) error {
	f, err := os.Create(filePath)
	if err != nil {
		return fmt.Errorf("could not create file '%s': %w", filePath, err)
	}
	defer f.Close()

	fmt.Fprintf(f, "P3\n%d %d\n255\n", img.width, img.height)
	for j := 0; j < img.height; j++ {
		for i := 0; i < img.width; i++ {
			c := img.at(i, j)
			fmt.Fprintf(f, "%d %d %d\n", c.R, c.G, c.B)
		}
	}

	return nil
}
