package pewpew

import "math/rand"

// A Scatterer has some effect on a camera ray, usually when it intersects an
// object.
type Scatterer interface {
	// Scatter gives the color attenuation (scale factor) and scatter direction
	// for a ray that intersected something. If the ray is absorbed then ok will
	// be false. This must be safe for concurrent use.
	Scatter(rnd *rand.Rand, incoming Ray, hit *Hit) (attenuation Color, scattered Ray, ok bool)

	// Emit gives the color of light emitted by a tangible object
	Emit(u, v float64, p Pos) Color
}
