package pewpew

import (
	"math"
	"math/rand"
	"sort"
)

// Intersecter is used for objects that can be intersected with rays.
type Intersecter interface {
	// Intersect checks if a ray intersects with the object, within the bounds for m.
	// It returns ok=true and a non-nil Intersect when a valid intersection is found.
	Intersect(r Ray, mMin float64, mMax float64) (hit *Hit, material Scatterer, ok bool)

	// BoundingBox calculates the rectangular bounding box aabb for an intersectable.
	BoundingBox(time0, time1 float64) AABB
}

// Comparison along the X axis between bounding boxes of two Intersecters.
func lessX(a, b Intersecter, time0, time1 float64) bool {
	aBox := a.BoundingBox(time0, time1)
	bBox := b.BoundingBox(time0, time1)

	return aBox.Min.X < bBox.Min.X
}

// Comparison along the Y axis between bounding boxes of two Intersecters.
func lessY(a, b Intersecter, time0, time1 float64) bool {
	aBox := a.BoundingBox(time0, time1)
	bBox := b.BoundingBox(time0, time1)

	return aBox.Min.Y < bBox.Min.Y
}

// Comparison along the Z axis between bounding boxes of two Intersecters.
func lessZ(a, b Intersecter, time0, time1 float64) bool {
	aBox := a.BoundingBox(time0, time1)
	bBox := b.BoundingBox(time0, time1)

	return aBox.Min.Z < bBox.Min.Z
}

// An AABB is an axis-aligned bounding box for intersectable objects.
//
// It is spans x_min to x_max along the X-axis, y_min to y_max along the Y-axis,
// and z_min to z_max along the Z-axis. It is specificed by two points at
// opposite corners of the box: (x_min, y_min, z_mix) and (x_max, y_max, z_max).
type AABB struct {
	Min, Max Pos
}

func (a AABB) intersect(r Ray, mMin, mMax float64) bool {
	invDX := 1 / r.Dir.X
	m0X := (a.Min.X - r.Origin.X) * invDX
	m1X := (a.Max.X - r.Origin.X) * invDX
	if invDX < 0 {
		m0X, m1X = m1X, m0X
	}
	mMin = math.Max(m0X, mMin)
	mMax = math.Min(m1X, mMax)
	if mMax <= mMin {
		return false
	}

	invDY := 1 / r.Dir.Y
	m0Y := (a.Min.Y - r.Origin.Y) * invDY
	m1Y := (a.Max.Y - r.Origin.Y) * invDY
	if invDY < 0 {
		m0Y, m1Y = m1Y, m0Y
	}
	mMin = math.Max(m0Y, mMin)
	mMax = math.Min(m1Y, mMax)
	if mMax <= mMin {
		return false
	}

	invDZ := 1 / r.Dir.Z
	m0Z := (a.Min.Z - r.Origin.Z) * invDZ
	m1Z := (a.Max.Z - r.Origin.Z) * invDZ
	if invDZ < 0 {
		m0Z, m1Z = m1Z, m0Z
	}
	mMin = math.Max(m0Z, mMin)
	mMax = math.Min(m1Z, mMax)

	return mMax > mMin
}

func surroundingBox(box0, box1 AABB) AABB {
	return AABB{
		Min: Pos{
			math.Min(box0.Min.X, box1.Min.X),
			math.Min(box0.Min.Y, box1.Min.Y),
			math.Min(box0.Min.Z, box1.Min.Z),
		},
		Max: Pos{
			math.Max(box0.Max.X, box1.Max.X),
			math.Max(box0.Max.Y, box1.Max.Y),
			math.Max(box0.Max.Z, box1.Max.Z),
		},
	}
}

type bvhNode struct {
	left       Intersecter
	right      Intersecter
	isLeftOnly bool // used for singleton nodes
	box        AABB
}

func newBVHNode(tangibles []Intersecter, time0, time1 float64) *bvhNode {
	// Choose comparison axis randomly
	var comparator func(a, b Intersecter, time0, time1 float64) bool
	switch rand.Intn(2 + 1) {
	case 0:
		comparator = lessX
	case 1:
		comparator = lessY
	default:
		comparator = lessZ
	}

	var left, right Intersecter
	switch len(tangibles) {
	case 1:
		left = tangibles[0]

		leftBox := left.BoundingBox(time0, time1)

		return &bvhNode{
			left:       left,
			isLeftOnly: true,
			box:        leftBox,
		}
	case 2:
		if comparator(tangibles[0], tangibles[1], time0, time1) {
			left, right = tangibles[0], tangibles[1]
		} else {
			left, right = tangibles[1], tangibles[0]
		}
	case 3:
		sort.Slice(tangibles, func(i, j int) bool {
			return comparator(tangibles[i], tangibles[j], time0, time1)
		})
		left, right = tangibles[0], newBVHNode(tangibles[1:], time0, time1)
	default:
		sort.Slice(tangibles, func(i, j int) bool {
			return comparator(tangibles[i], tangibles[j], time0, time1)
		})
		midInd := len(tangibles) / 2
		left, right = newBVHNode(tangibles[:midInd], time0, time1), newBVHNode(tangibles[midInd:], time0, time1)
	}

	leftBox := left.BoundingBox(time0, time1)
	rightBox := right.BoundingBox(time0, time1)

	return &bvhNode{
		left:  left,
		right: right,
		box:   surroundingBox(leftBox, rightBox),
	}
}

func (b *bvhNode) Intersect(r Ray, mMin float64, mMax float64) (hit *Hit, material Scatterer, ok bool) {
	if !b.box.intersect(r, mMin, mMax) {
		return // no hit
	}

	if hit, material, ok = b.left.Intersect(r, mMin, mMax); ok {
		mMax = hit.M // left hit, check if right hit is closer than left
	}

	if b.isLeftOnly {
		return // don't bother checking right if it is empty
	}

	if hitRight, materialRight, okRight := b.right.Intersect(r, mMin, mMax); okRight {
		hit = hitRight
		material = materialRight
		ok = true
	}

	return
}

func (b *bvhNode) BoundingBox(time0, time1 float64) AABB { return b.box }
