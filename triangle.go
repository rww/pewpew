package pewpew

import "math"

type Triangle struct {
	p0, p1, p2   Pos
	edge1, edge2 Vec
	normal       Vec
	material     Scatterer
}

func NewTriangle(p0, p1, p2 Pos, material Scatterer) *Triangle {
	edge1 := p0.to(p1)
	edge2 := p0.to(p2)
	normal := edge1.crossProduct(edge2).norm()
	return &Triangle{p0, p1, p2, edge1, edge2, normal, material}
}

// Intersect checks for an intersection of a ray with a triangle, within bounds.
func (t *Triangle) Intersect(r Ray, mMin float64, mMax float64) (hit *Hit, material Scatterer, ok bool) {
	h := r.Dir.crossProduct(t.edge2)
	a := t.edge1.dot(h)

	if -0.00001 < a && a < 0.00001 {
		return // ray is parallel to triangle, no hit
	}

	f := 1 / a
	s := t.p0.to(r.Origin)
	u := f * s.dot(h)

	if u < 0 || 1 < u {
		return // hit is outside triangle
	}

	q := s.crossProduct(t.edge1)
	v := f * r.Dir.dot(q)

	if v < 0 || 1 < u+v {
		return // hit is outside triangle
	}

	m := f * t.edge2.dot(q)

	if m < mMin || mMax < m {
		return // hit is outside range bounds along ray
	}

	hitP := r.at(m)
	hit = NewHitFromOutwardNormal(hitP, t.normal, m, u, v, r)

	return hit, t.material, true
}

// BoundingBox generates the bounding box for a triangle.
func (t *Triangle) BoundingBox(time0, time1 float64) AABB {
	minX, maxX := paddedMinMaxOf3(t.p0.X, t.p1.X, t.p2.X)
	minY, maxY := paddedMinMaxOf3(t.p0.Y, t.p1.Y, t.p2.Y)
	minZ, maxZ := paddedMinMaxOf3(t.p0.Z, t.p1.Z, t.p2.Z)

	return AABB{
		Min: Pos{minX, minY, minZ},
		Max: Pos{maxX, maxY, maxZ},
	}
}

// Returns the smallest and largest of 3 numbers.
func minMaxOf3(a, b, c float64) (min, max float64) {
	if a < b && a < c {
		return a, math.Max(b, c)
	}

	if b < a && b < c {
		return b, math.Max(a, c)
	}

	return c, math.Max(a, b)
}

// Returns the smallest and largest of 3 numbers, padding slightly if they are
// equal.
func paddedMinMaxOf3(a, b, c float64) (min, max float64) {
	min, max = minMaxOf3(a, b, c)
	if min == max {
		return min - 0.00001, max + 0.00001
	}
	return
}
