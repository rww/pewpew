package pewpew

// A Hit describes an intersection of a ray with a tangible object.
type Hit struct {
	Point       Pos
	Normal      Vec     // by convention, this always points against the ray
	M           float64 // multiple of the ray
	U, V        float64 // surface coordinates of the hit on the object
	OutsideFace bool    // if the hit struck the outside face of the object
}

// NewHitFromOutwardNormal defines a new Hit, calculating the against-ray normal
// from the intersected object's outward normal.
func NewHitFromOutwardNormal(point Pos, outwardNormal Vec, m, u, v float64, r Ray) *Hit {
	hitOutside := r.Dir.dot(outwardNormal) < 0.0
	if !hitOutside {
		// If ray hit the inward surface, reverse normal to match the convention
		// of keeping the normal pointing against the ray
		outwardNormal = outwardNormal.rev()
	}
	return &Hit{
		Point:       point,
		Normal:      outwardNormal,
		M:           m,
		U:           u,
		V:           v,
		OutsideFace: hitOutside,
	}
}
