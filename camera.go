package pewpew

import (
	"math"
	"math/rand"
)

const (
	defaultAspectRatio = 1
	defaultVerticalFOV = 20
)

// A Camera describes the viewpoint for rendering a scene.
type Camera struct {
	origin               Pos
	lowerLeftCorner      Pos
	horizontal, vertical Vec
	u, v, w              Vec
	useFocus             bool
	lensRadius           float64
	useTime              bool
	startTime, endTime   float64
}

// NewCamera creates a new viewpoint for a scene with the given positioning.
//
// The default camera uses an aspect ratio of 1:1 with a 20° vertical field of
// view and an infinitessimal camera aperture (no focusing effects). The default
// shutter timing is instantaneous at time=0.
//
// CameraOptions can be used for modifying the camera from the defaults.
func NewCamera(position Pos, target Pos, options ...CameraOption) *Camera {
	// Default setup
	setup := defaultCameraSetup()

	// Apply options to setup
	for _, option := range options {
		option(setup)
	}

	// Calculate values based on setup
	w := target.to(position).norm() // depth from camera is negative by convention

	upDirection := Vec{0, 1, 0} // default 'upward' orientation is positive Y
	u := Vec{0, 0, -1}
	if w != upDirection { // prevent divide by zero, default 'upward' orientation is negative Z if camera is already pointing 'up'
		u = upDirection.crossProduct(w).norm()
	}
	if setup.rollAngle != 0 {
		// Apply roll about camera direction (w)
		u = u.rotateAbout(w, -setup.rollAngle) // negative sign because w is opposite to direction camera is pointing
	}
	v := w.crossProduct(u)

	focusDistance := position.to(target).length()
	if setup.useFocusDistance {
		focusDistance = setup.focusDistance
	}

	horizontal := u.scale(focusDistance * setup.viewPortWidth)
	vertical := v.scale(focusDistance * setup.viewPortHeight)
	llc := position.
		subV(horizontal.scale(0.5)).
		subV(vertical.scale(0.5)).
		subV(w.scale(focusDistance))

	return &Camera{
		origin:          position,
		lowerLeftCorner: llc,
		horizontal:      horizontal,
		vertical:        vertical,
		u:               u,
		v:               v,
		w:               w,
		useFocus:        (setup.lensRadius > 0),
		lensRadius:      setup.lensRadius,
		useTime:         (setup.startTime != setup.endTime),
		startTime:       setup.startTime,
		endTime:         setup.endTime,
	}
}

// getRay creates a ray from a viewpoint.
func (c *Camera) getRay(rnd *rand.Rand, s, t float64) Ray {
	target := c.lowerLeftCorner.
		addV(c.horizontal.scale(s)).
		addV(c.vertical.scale(t))

	origin := c.origin
	if c.useFocus {
		pertur := randomVInUnitCircle(rnd).scale(c.lensRadius)
		offset := c.u.scale(pertur.X).add(c.v.scale(pertur.Y))
		origin = origin.addV(offset)
	}

	time := float64(0)
	if c.useTime {
		time = (c.endTime-c.startTime)*rnd.Float64() + c.startTime
	}

	return newRayToPosition(origin, target, time)
}

// A CameraOption is used to modify a Camera during setup.
type CameraOption func(s *cameraSetup)

// Aperture modifies the simulated aperture diameter of the camera.
func Aperture(aperture float64) CameraOption {
	return func(s *cameraSetup) { s.lensRadius = aperture / 2 }
}

// FieldOfView modifies a camera's field of view.
//
// Aspect ratio is the ratio of the camera's width to height.
// VerticalAngleDegrees is the field of view angle in the camera's vertical
// direction.
func FieldOfView(aspectRatio, verticalAngleDegrees float64) CameraOption {
	return func(s *cameraSetup) {
		s.setViewPortSize(aspectRatio, verticalAngleDegrees)
	}
}

// Focus modifies the focus of the camera to be a distance depth from its
// origin.
func Focus(depth float64) CameraOption {
	return func(s *cameraSetup) {
		s.useFocusDistance = true
		s.focusDistance = depth
	}
}

// Roll rotates the camera about the direction it is pointing, a positive angle
// rotates to the right, negative rotates to the left.
func Roll(angleDegrees float64) CameraOption {
	return func(s *cameraSetup) { s.rollAngle = angleDegrees }
}

// ShutterSpeed adds shutter timing to the camera, for adding motion blur from
// moving objects.
func ShutterSpeed(start, duration float64) CameraOption {
	return func(s *cameraSetup) { s.setShutterTime(start, duration) }
}

type cameraSetup struct {
	rollAngle        float64 // degrees
	viewPortHeight   float64
	viewPortWidth    float64
	useFocusDistance bool
	focusDistance    float64
	lensRadius       float64
	startTime        float64
	endTime          float64
}

func defaultCameraSetup() *cameraSetup {
	setup := &cameraSetup{}
	setup.setViewPortSize(defaultAspectRatio, defaultVerticalFOV)
	return setup
}

func (s *cameraSetup) setViewPortSize(aspectRatio, verticalFOVDegrees float64) {
	s.viewPortHeight = 2 * math.Tan(degToRad(verticalFOVDegrees/2))
	s.viewPortWidth = aspectRatio * s.viewPortHeight
}

func (s *cameraSetup) setShutterTime(start float64, duration float64) {
	s.startTime = start
	s.endTime = start + duration
}

func degToRad(deg float64) float64 {
	const factor = math.Pi / 180
	return deg * factor
}
